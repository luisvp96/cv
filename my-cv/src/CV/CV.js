export const CV = {
    hero: {
      name: "Luis",
      adress: "Vaz Pinto",
      city: "Lisbon, Portugal",
      email: "lvazpinto96@gmail.com",
      birthDate: "04/06/1996",
      phone: "+351 911 111 119",
      image: "https://images.unsplash.com/photo-1602992708529-c9fdb12905c9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
      gitHub: "https://gitlab.com/luisvp96",
      aboutMe: [
        {
          info: "I like elctronic music and prog rock",
        },
        {
          info: "Passionate about technology and coding",
        },
        {
          info: "I love football and sometimes it gets on my nerves",
        },
        {
          info: "I do have a responsibility to keep my code clean",
        },
      ],
    },
    education: [
      {
        name: "BA Corporate Finance",
        date: "2020",
        where: "ISCAL",
      },
      {
        name: "Full Stack Web Development Bootcamp",
        date: "2021",
        where: "Le Wagon",
      },
      {
        name: "Piscine",
        date: "20202",
        where: "42 Lisbon",
      },
    ],
    experience: [
      {
        name: "Intern",
        date: "05/01/2020 – 5/5/2020",
        where: "RPLM",
        description:
          "Accouting intern",
      },
      {
        name: "Operations manager",
        date: "01/06/2018 – 1/03/2020",
        where: "IMPEC Events",
        description:
          "Manage the logistic operation of the company that has meny events evry week.",
      },
    ],
    languages: {
      language: "English",
      wrlevel: "Native",
      splevel: "Native",
    },
    habilities: [
      "Italian",
      "Spanish",
      "Ruby on Rails",
      "React",
      "SQL",
      "HTML",
      "CSS",
      "JavaScript",
    ],
    volunteer: [
      {
        name: "Banco Alimentar Contra a Fome",
        where: "Lisbon",
        description:
          "The Food Banks in activity collect and distribute several tens of thousands of tons of products and support throughout the year, the action of institutions in Portugal. In turn, these distribute prepared meals and food baskets to people who are proven to be in need, already covering a total distribution of more than 390,000 people.",
      },
      {
        name: "Oh Gui",
        where: "Cascais",
        description:
          "Helping and teaching handicaped children play rugby.",
      },
    ],
  };