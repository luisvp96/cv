import React from "react";
import "./Footer.css";

export const Footer = () => {
  return (
    <div>
      <div className="Footer">
        <p className="p-footer">This footer was made by Luis Vaz Pinto</p>
      </div>
    </div>
  );
};

export default Footer;