import { useState } from "react";
import "./App.css";
import { Hero } from "./Components/Hero";
import { Education } from "./Components/Education";
import { Experience } from "./Components/Experience";
import { CV } from "./CV/CV";
import { Footer } from "./Components/Footer";


const { hero, education, experience} = CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);
  return (
    <div>
      <div className="App">
          <Hero hero={hero} />
          <button
                className="custom-btn btn-4"
                onClick={() => setShowEducation(true)}
              >
                Education
              </button>
              <button
                className="custom-btn btn-4"
                onClick={() => setShowEducation(false)}
              >
                Experience
              </button>
            <div>
              {showEducation ? (
                <Education education={education} />
              ) : (
                <Experience experience={experience} />
              )}
            </div>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
